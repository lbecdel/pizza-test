# pizza-test
Installation

If needed, modify .env with the correct database information

    DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/
    
Create Database if not existing

    php bin/console doctrine:database:create
    
Create schema
    
    php bin/console doctrine:schema:create
   
Load sample data

    php bin/console doctrine:fixtures:load
