<?php

namespace App\DataFixtures;

use App\Entity\Ingredient;
use App\Entity\IngredientOrder;
use App\Entity\Pizza;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $tomato = new Ingredient();
        $tomato->setName('tomato');
        $tomato->setCost(0.5);
        $mushrooms = new Ingredient();
        $mushrooms->setName('sliced mushrooms');
        $mushrooms->setCost(0.5);
        $feta = new Ingredient();
        $feta->setName('feta cheese');
        $feta->setCost(1.0);
        $sausages = new Ingredient();
        $sausages->setName('sausages');
        $sausages->setCost(1.0);
        $onion = new Ingredient();
        $onion->setName('sliced onion');
        $onion->setCost(0.5);
        $mozza = new Ingredient();
        $mozza->setName('mozzarella cheese');
        $mozza->setCost(0.5);
        $oregano = new Ingredient();
        $oregano->setName('oregano');
        $oregano->setCost(1);
        $bacon = new Ingredient();
        $bacon->setName('bacon');
        $bacon->setCost(1);

        $manager->persist($tomato);
        $manager->persist($mushrooms);
        $manager->persist($feta);
        $manager->persist($sausages);
        $manager->persist($onion);
        $manager->persist($mozza);
        $manager->persist($oregano);
        $manager->persist($bacon);

        $funPizza = new Pizza();
        $funPizza->setName('Fun Pizza');
        $funPizza->setPrice(7.5);

        $manager->persist($funPizza);

        $oTomato = new IngredientOrder();
        $oTomato->setIngredient($tomato);
        $oTomato->setWeight(1);
        $oMushrooms = new IngredientOrder();
        $oMushrooms->setIngredient($mushrooms);
        $oMushrooms->setWeight(2);
        $oFeta = new IngredientOrder();
        $oFeta->setIngredient($feta);
        $oFeta->setWeight(3);
        $oSausages = new IngredientOrder();
        $oSausages->setIngredient($sausages);
        $oSausages->setWeight(4);
        $oOnion = new IngredientOrder();
        $oOnion->setIngredient($onion);
        $oOnion->setWeight(5);
        $oMozza = new IngredientOrder();
        $oMozza->setIngredient($mozza);
        $oMozza->setWeight(6);
        $oOregano = new IngredientOrder();
        $oOregano->setIngredient($oregano);
        $oOregano->setWeight(7);

        $manager->persist($oTomato);
        $manager->persist($oMushrooms);
        $manager->persist($oFeta);
        $manager->persist($oSausages);
        $manager->persist($oOnion);
        $manager->persist($oMozza);
        $manager->persist($oOregano);

        $funPizza->addIngredient($oTomato);
        $funPizza->addIngredient($oMushrooms);
        $funPizza->addIngredient($oFeta);
        $funPizza->addIngredient($oSausages);
        $funPizza->addIngredient($oOnion);
        $funPizza->addIngredient($oMozza);
        $funPizza->addIngredient($oOregano);

        $manager->persist($funPizza);

        $mushroomPizza = new Pizza();
        $mushroomPizza->setName('The Super Mushroom Pizza');
        $mushroomPizza->setPrice(5.25);

        $manager->persist($mushroomPizza);

        $orderTomato = new IngredientOrder();
        $orderTomato->setIngredient($tomato);
        $orderTomato->setWeight(1);
        $orderBacon = new IngredientOrder();
        $orderBacon->setIngredient($bacon);
        $orderBacon->setWeight(2);
        $orderMozza = new IngredientOrder();
        $orderMozza->setIngredient($mozza);
        $orderMozza->setWeight(3);
        $orderMushrooms = new IngredientOrder();
        $orderMushrooms->setIngredient($mushrooms);
        $orderMushrooms->setWeight(4);
        $orderOregano = new IngredientOrder();
        $orderOregano->setIngredient($oregano);
        $orderOregano->setWeight(5);

        $manager->persist($orderTomato);
        $manager->persist($orderBacon);
        $manager->persist($orderMozza);
        $manager->persist($orderMushrooms);
        $manager->persist($orderOregano);
        $mushroomPizza->addIngredient($orderTomato);
        $mushroomPizza->addIngredient($orderBacon);
        $mushroomPizza->addIngredient($orderMozza);
        $mushroomPizza->addIngredient($orderMushrooms);
        $mushroomPizza->addIngredient($orderOregano);

        $manager->persist($mushroomPizza);

        $manager->flush();
    }
}
