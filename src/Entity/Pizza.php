<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PizzaRepository")
 */
class Pizza
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="IngredientOrder", mappedBy="pizza",cascade={"persist", "remove"}))
     */
    private $ingredients;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    public function __construct()
    {
        $this->ingredients = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getIngredients()
    {
        return $this->ingredients;
    }

    /**
     * @param IngredientOrder $ingredientOrder
     */
    public function addIngredient($ingredientOrder)
    {
        $ingredientOrder->setPizza($this);
        $this->ingredients->add($ingredientOrder);
    }

    public function removeIngredient($ingredientOrder)
    {
        $this->ingredients->removeElement($ingredientOrder);
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }
}
