<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IngredientOrderRepository")
 */
class IngredientOrder
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $weight;

    /**
     * @ORM\ManyToOne(targetEntity="Pizza", inversedBy="ingredients")
     * @ORM\JoinColumn(name="pizza_id", referencedColumnName="id")
     */
    private $pizza;

    /**
     * @ORM\ManyToOne(targetEntity="Ingredient", inversedBy="ingredients")
     * @ORM\JoinColumn(name="ingredient_id", referencedColumnName="id")
     *
     */
    private $ingredient;


    public function getId()
    {
        return $this->id;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPizza()
    {
        return $this->pizza;
    }

    /**
     * @param mixed $pizza
     */
    public function setPizza($pizza)
    {
        $this->pizza = $pizza;
    }

    /**
     * @return Ingredient
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }

    /**
     * @param mixed $ingredient
     */
    public function setIngredient($ingredient)
    {
        $this->ingredient = $ingredient;
    }

}
