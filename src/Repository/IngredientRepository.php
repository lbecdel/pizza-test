<?php

namespace App\Repository;

use App\Entity\Ingredient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Ingredient|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ingredient|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ingredient[]    findAll()
 * @method Ingredient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IngredientRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ingredient::class);
    }

    public function findByPizzaOrdered($pizzaId)
    {
        return $this->createQueryBuilder('i')
            ->join('i.ingredientsOrder', 'io')
            ->andWhere('io.pizza = :id_pizza')
            ->setParameter('id_pizza', $pizzaId)
            ->orderBy('io.weight', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
