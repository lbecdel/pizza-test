<?php

namespace App\Controller;

use App\Entity\Ingredient;
use App\Entity\IngredientOrder;
use App\Entity\Pizza;
use App\Form\ReceiptFormType;
use App\Repository\IngredientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    const TAXE = 1.5;
    /**
     * @Route("/", name="default")
     */
    public function index()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $pizzaRepo = $entityManager->getRepository(Pizza::class);
        $pizzas = $pizzaRepo->findAll();

        $entityManager = $this->getDoctrine()->getManager();
        /** @var IngredientRepository $ingredientRepo */
        $ingredientRepo = $entityManager->getRepository(Ingredient::class);
        $listPizza = [];
        /** @var Pizza $pizza */
        foreach ($pizzas as $pizza) {
            $listPizza[] = [
                'ingredients' =>
                    $pizzaIngredientsOrdered = $ingredientRepo->findByPizzaOrdered($pizza->getId()),
                'name' => $pizza->getName(),
                'price' => $pizza->getPrice(),
                'id' => $pizza->getId()
            ];
        }

        return $this->render('default/index.html.twig', [
            'pizzas' => $listPizza
        ]);
    }

    /**
     * @Route("/pizza/create", name="pizza_creation")
     * @Route("/pizza/{id}", name="pizza_edit")
     */
    public function editPizza(Request $request, Pizza $pizza = null)
    {
        $title = 'Modify Pizza';
        if ($pizza === null) {
            $pizza = new Pizza();
            $title = 'Creation Pizza';
        }
        $originalIngredients = new ArrayCollection();
        foreach ($pizza->getIngredients() as $ingredient) {
            $originalIngredients->add($ingredient);
        }

        $form = $this->createForm(ReceiptFormType::class, $pizza);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            /** @var IngredientOrder $originalIngredient */
            foreach ($originalIngredients as $originalIngredient) {
                if (false === $pizza->getIngredients()->contains($originalIngredient)) {
                    $entityManager->remove($originalIngredient);
                }
            }
            $pizza = $this->calculatePrice($pizza);
            $entityManager->persist($pizza);
            $entityManager->flush();

            return $this->redirectToRoute('default');
        }

        return $this->render('default/edit.html.twig', [
            'form' => $form->createView(),
            'title' => $title
        ]);
    }


    private function calculatePrice(Pizza $pizza)
    {
        $price = 0;
        /** @var IngredientOrder $ingredient */
        foreach ($pizza->getIngredients() as $ingredient) {
            $price += $ingredient->getIngredient()->getCost();
        }
        $pizza->setPrice($price * self::TAXE );
        return $pizza;
    }
}
